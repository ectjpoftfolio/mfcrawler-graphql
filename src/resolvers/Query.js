async function test(parent, args, context, info) {
  const users = await context.db.query.users({}, `{ id email }`);

  return users;
}

async function funds(parent, args, context, info) {
  const funds = await context.db.query.funds({}, `{ id abbreviation name }`);

  return funds.map((fund) => {

    const fundWithPrices = {
      id: fund.id,
      abbreviation: fund.abbreviation,
      name: fund.name,
    };

    return fundWithPrices;
  });

}

async function transactions(parent, args, context, info) {
  const transactions = await context.db.query.transactions({ where: { user: { id: args.userId } } }, `{ id amount netAmount, nav, date, fund { id }, user { id } }`);

  return transactions.map((transaction) => {
    const detailedTansactions = {
      id: transaction.id,
      amount: transaction.amount,
      netAmount: transaction.netAmount,
      nav: transaction.nav,
      date: transaction.date,
      fundId: transaction.fund.id,
      userId: transaction.user.id,
    };

    return detailedTansactions;
  });
}

async function user(parent, args, context, info) {
  if (!!context.request.session.user) {
    return {
      id: context.request.session.user.id,
      email: context.request.session.user.email
    }
  }
}

module.exports = {
  test,
  funds,
  transactions,
  user,

}