async function prices(parent, args, context, info) {
  const fundPrices = await context.db.query.fundPrices({ where: { id: parent.id } }, info);

  return fundPrices;
}

module.exports = {
  prices,
}