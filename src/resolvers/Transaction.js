async function fund(parent, args, context, info) {
  const fund = await context.db.query.fund({ where: { id: parent.fundId } }, `{ id abbreviation name }`);

  return fund;
}

async function user(parent, args, context, info) {
  const user = await context.db.query.user({ where: { id: parent.userId } }, `{ id email }`);

  return user;
}

module.exports = {
  user,
  fund,
}