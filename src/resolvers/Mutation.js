const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils')
const passport = require('passport');

async function signup(parent, args, context, info) {
  if (!args.email || !args.password) { throw new Error('You must provide an email and password.'); }

  const emailUsed = await context.db.query.user({ where: { email: args.email.toLowerCase() } }, `{ id }`);

  if (emailUsed) {
    throw new Error('Email has been used');
  }

  const password = await new Promise((resolve, reject) => {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) { reject(err); }
      bcrypt.hash(args.password, salt, function (err, hash) {
        if (err) { reject(err); }
        resolve(hash);
      });
    });
  });

  const user = await context.db.mutation.createUser({
    data: { email: args.email.toLowerCase(), password }
  }, `{ id email }`);

  return new Promise((resolve, reject) => {
    context.req.login(user, (err) => {
      if (err) { reject(err); }
      resolve(user);
    });
  });
}

async function login(parent, args, context, info) {
  context.req.session.awfawef = 'awefawefawf';
  return new Promise((resolve, reject) => {
    passport.authenticate('local', (err, user) => {
      if (!user) { reject('Invalid credentials.') }
      context.request.session.user = {
        id: user.id,
        email: user.email,
      };
      resolve(user);
    })({ body: { email: args.email, password: args.password }, context });
  });
}

async function logout(parent, args, context, info) {
  const { user } = context.req.session;
  if (!user) {
    throw new Error('You are not logged in')
  }
  context.request.session.user = null;
  context.req.logout();
  return user.id;
}

async function upsertFundsAndPrices(parent, args, context, info) {

  const parsedScrapdata = JSON.parse(args.scrapData);

  parsedScrapdata.map(async (fund) => {
    const existingFund = await context.db.query.funds({ where: { abbreviation: fund.abbreviation, name: fund.name } })

    let itFund;

    if (existingFund.length < 1) {
      itFund = await context.db.mutation.createFund({ data: { name: fund.name, abbreviation: fund.abbreviation } });
    } else {
      console.log('existingFund', existingFund);
      itFund = existingFund[0];
    }

    const fundPrice = await context.db.query.fundPrices({
      where: {
        AND:
          [{ fund: { id: itFund.id } },
          { date: fund.date }]
      }
    });

    if (fundPrice.length < 1) {
      await context.db.mutation.createFundPrice({ data: { nav: fund.nav, date: fund.date, fund: { connect: { id: itFund.id } } } });
    }
  });

  const funds = await context.db.query.funds({}, `{ id abbreviation name }`);
  return funds;
}

async function createTransaction(parent, args, context, info) {

  const transaction = await context.db.mutation.createTransaction({
    data: {
      amount: args.amount,
      netAmount: args.netAmount,
      nav: args.nav,
      date: new Date(args.date),
      fund: { connect: { id: args.fundId } },
      user: { connect: { id: args.userId } },
    }
  });

  if (!transaction) {
    throw new Error('Failed to add transaction');
  }

  return transaction.id;
}

module.exports = {
  signup,
  login,
  logout,
  upsertFundsAndPrices,
  createTransaction,
}