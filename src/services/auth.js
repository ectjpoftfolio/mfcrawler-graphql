const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const server = require('../index');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

// this isn't used. Using own sessions
passport.deserializeUser((id, done) => {
  server.db.query.user({ where: { id } }, `{ id email }`).then((user) => {
    done(err, user);
  });
});

passport.use(new LocalStrategy({ usernameField: 'email', passReqToCallback: true }, (req, email, password, done) => {
  req.context.db.query.user({ where: { email: email.toLowerCase() } }, `{ id email password }`).then((user) => {
    if (!user) { return done(null, false, 'User Not Found'); }
    bcrypt.compare(password, user.password, function (err, res) {
      if (res == true) {
        done(null, user);
      } else {
        return done(null, false, 'Invalid credentials.');
      }
    });
  }).catch((error) => {
    return done(err);
  });
}));
