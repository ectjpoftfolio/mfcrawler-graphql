const { GraphQLServer } = require('graphql-yoga');
const { Prisma } = require('prisma-binding');
const { GraphQLJSON } = require('graphql-type-json');
const passport = require('passport');
const session = require('express-session');
const flash = require('connect-flash');

require('dotenv').config();
require('./services/auth');

const Mutation = require('./resolvers/Mutation');
const Query = require('./resolvers/Query');
const AuthPayload = require('./resolvers/AuthPayload');
const Fund = require('./resolvers/Fund');
const Transaction = require('./resolvers/Transaction');

const resolvers = {
  Mutation,
  Query,
  AuthPayload,
  Fund,
  Transaction,
};

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: req => ({
    req: req.request,
    ...req,
    db: new Prisma({
      typeDefs: 'src/generated/prisma.graphql',
      endpoint: 'http://localhost:4467',
      secret: 'memeluvu',
      debug: true,
    })
  })
});

server.express.use(session({
  name: 'mfqq',
  secret: 'memeluvsession',
  resave: true,
  saveUninitialized: true,
  cookie: {
    secure: process.env.NODE_ENV === 'production',
    maxAge: 1000 * 60 * 60 * 24 * 2,
  },
}));

server.express.use(passport.initialize());
server.express.use(passport.session());
server.express.use(flash());

const opts = {
  port: 4001,
  cors: {
    credentials: true,
    origin: ['http://localhost:3000'] // your frontend url.
  },
  debug: true,
};

server.start(opts, () => console.log(`Server is running on http://localhost:4000`));

module.exports = { db: server.context.db };